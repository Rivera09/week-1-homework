const img = document.querySelector(".img-container img");

let showing = false;
const code = ["1", "2", "3"];
const sources = ["./img/snake.gif", "./img/contra.gif", "./img/pacman.gif"];
let index = 0;

window.onload = () => {
  alert("check the console to see how to activate the easter egg");
  console.log(`Press the sequence ${code.join(" ")}`);
};

document.onkeypress = (e) => {
  if (showing) return;
  if (e.key.toLowerCase() !== code[index]) return (index = 0);
  index++;
  if (index !== code.length) return;
  showGif();
  index = 0;
};

const showGif = () => {
  if (showing) return;
  const random = Math.floor(Math.random() * 3);
  showing = true;
  img.setAttribute("src", sources[random]);
  setTimeout(() => {
    img.removeAttribute("src");
    showing = false;
  }, 1000);
};
