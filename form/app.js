const form = document.getElementById("signUpForm");
const inputs = document.querySelectorAll(".form-field input");
const password = document.querySelector("#password");
const confirmPassword = document.querySelector("#confirmPassword");
const signUPBtn = document.getElementById("signUpBtn");
const checkbox = document.querySelector("#checkbox");

signUPBtn.disabled = true;

let errors = 0;

const regexes = {
  firstName: [
    new RegExp("^[a-zA-Záéíóú]+ *([a-zA-Záéíóú]+) *$"),
    "Only characters allowed",
  ],
  lastName: [
    new RegExp("^[a-zA-Záéíóú]+ *([a-zA-Záéíóú]+) *$"),
    "Only characters allowed",
  ],
  email: [
    new RegExp("^[\\w\\-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$"),
    "Email is not valid",
  ],
  password: [
    new RegExp("^.{6,}$"),
    "Passoword must be al leats 6 characters long",
  ],
  code: [new RegExp("^\\+*[0-9]{3}$"), ""],
  phoneNumber: [new RegExp("^[0-9]{4}-?[0-9]{4}$"), "Invalid phone number"],
  age: [new RegExp("^[0-9]{1,3}$"), "Invalid age"],
  website: [
    new RegExp(
      "^https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)$"
    ),
    "Invalid URL",
  ],
};

const showErrors = (condition, classList, elementId, errMsg) => {
  if (condition) {
    if (classList.contains("error")) {
      document.getElementById(elementId).innerText = "";
      classList.remove("error");
      errors--;
    }
    if (!classList.contains("ok")) classList.add("ok");
    return false;
  } else {
    if (classList.contains("ok")) classList.remove("ok");
    if (!classList.contains("error")) {
      errors++;
      classList.add("error");
      document.getElementById(elementId).innerText = errMsg;
    }
    // True because it showed an error
    return true;
  }
};

for (let input of inputs) {
  input.addEventListener("focusout", (e) => {
    const { name, value, classList } = e.target;

    // I don't want to apply any validation when modifying the range input.
    if (name === "expertise") return;

    // If there's no value, there's nothing to validate
    if (!value)
      return showErrors(false, classList, `${name}ErrMsg`, "Required");

    // Remove the requierd message
    showErrors(true, classList, `${name}ErrMsg`, "Requiered");

    // I don't want to validate any regex when typing on confirm password.
    if (name === "confirmPassword" && password.value !== "")
      return showErrors(
        value === password.value,
        classList,
        `${name}ErrMsg`,
        "Passwords don't match"
      );

    // If the confirm password field has been filled before
    if (name === "password" && confirmPassword.value !== "")
      showErrors(
        value === confirmPassword.value,
        confirmPassword.classList,
        "confirmPasswordErrMsg",
        "Passwords don't match"
      );

    if (name === "firstName" || name === "lastName")
      showErrors(
        value.length > 1,
        classList,
        `${name}ErrMsg`,
        "Only one character not allowed"
      );

    if (
      name === "age" &&
      (showErrors(+value <= 120, classList, `${name}ErrMsg`, "Max age: 120") ||
        showErrors(+value > 0, classList, `${name}ErrMsg`, "Min age: 1"))
    ) {
      console.log("errors", errors);
      return;
    }

    // General cases
    if (
      regexes[name] &&
      showErrors(
        regexes[name][0].test(value),
        classList,
        `${name}ErrMsg`,
        regexes[name][1]
      )
    );

  });
}

form.addEventListener("submit", (e) => {
  e.preventDefault();
  document.activeElement.blur();
  let output = "";
  if (errors > 0) return alert("Validate your form data");

  for (let input of inputs) {
    if (input.value === "") return alert("No empty fields are allowed");
    output += `${input.name}: ${input.value} \n`;
  }
  console.log(output);
  alert("Data sent successfully. Check the console.");
});

checkbox.addEventListener("click", () => {
  signUPBtn.classList.toggle("disabled");
  signUPBtn.disabled = !checkbox.checked;
});
